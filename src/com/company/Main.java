package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {


        Scanner name1 = new Scanner(System.in);
        Scanner age1 = new Scanner(System.in);
        Scanner temp1 = new Scanner(System.in);
        System.out.println("Имя человека:");
        String name = name1.nextLine();
        System.out.println("Возвраст человека:");
        int age = age1.nextInt();
        System.out.println("Температура на улице на сегодня:");
        int temp = temp1.nextInt();


        if (age > 20 && age < 45) {     //1 условия
            if (temp > 30 || temp < -20) {
                System.out.println(name + " не выходит гулять");
            } else {
                System.out.println(name + " отправляется в гости к своему другу ");
            }
        } else if (age < 20) {    //2 условия
            if (temp > 0 && temp < 28) {
                System.out.println(name + " не выходит гулять");
            } else {
                System.out.println(name + " отправляется в гости к своему другу ");
            }
        } else if (age > 45) {   //3 условия
            if (temp > -10 && temp < 25) {
                System.out.println(name + " не выходит гулять");
            } else {
                System.out.println(name + " отправляется в гости к своему другу ");
            }
        }
    }
}

